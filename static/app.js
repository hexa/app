var a;
var c;

$.getJSON("overview.json", function (data) {
    a = data;
    var communities = Object.keys(data);
    var network_worth = communities.map(function (key, i) {
        return data[key].loss;
    });
    var models = communities.map(function (key, i) {
        return data[key].models;
    });

    var ctx = document.getElementById("indexChart").getContext('2d');
    var indexChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: communities,
            datasets: [{
                label: 'Gesamtwert der Legacy-Freifunk-Router',
                data: network_worth,
                backgroundColor: 'rgba(220,0,103,0.2)',
                borderColor: 'rgba(220,0,103,1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            onClick: function (e, chartObject) {
                if (chartObject.length !== 0) {
                    var name = chartObject[0]._model.label;
                    location.hash = name;
                    setData(name, models[chartObject[0]._index]);
                    showPane('community');
                }
            },
            onHover: function (e, el) {
                var section = el[0];
                e.currentTarget.style.cursor = section ? 'pointer' : 'default';
            }
        }
    });

    var originalShowTooltip = indexChart.showTooltip;
    indexChart.showTooltip = function (activeElements) {
        $("#dc_LoadTime").css("cursor", activeElements.length ? "pointer" : "default");
        originalShowTooltip.apply(this, arguments);
    };

    if (location.hash) {
        var name = location.hash.substring(1);
        setData(name, models[getKeyByValue(communities, name)]);
        showPane('community');
    }
});

function getKeyByValue(list, value) {
    var key = undefined;

    list.forEach(function (v, k) {
        if (value !== v) {
            return;
        }

        key = k;
    });

    return key;
}

function setData(name, models) {
    var devices = Object.keys(models);
    var device_values = devices.map(function (key, i) {
        return models[key].loss;
    });
    var ctx = document.getElementById("communityChart").getContext('2d');
    var config = {
        type: 'pie',
        data: {
            labels: devices,
            datasets: [{
                label: 'Wert der Geräte dieses Typs',
                data: device_values,
                backgroundColor: 'rgba(255,180,0,0.2)',
                borderColor: 'rgba(255,180,0,1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
        },
    };

    $('.text-communityName').text(name);

    if (!c) {
        c = new Chart(ctx, config);
    } else {
        c.data.labels = config.data.labels;
        c.data.datasets = config.data.datasets;
        c.update();
    }
}

document.querySelector('.a-gohome').addEventListener('click', function () {
    showPane('index');
});

function showPane(name) {
    var panes = document.querySelectorAll('.pane');
    for (var i = 0; i < panes.length; i++) {
        panes[i].classList.remove("show");
    }
    document.querySelector('.pane.pane-' + name).classList.add('show');
}
