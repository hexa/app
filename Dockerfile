FROM python:3-alpine

WORKDIR /app

COPY requirements.txt /app
RUN apk add --no-cache git && pip3 install -r requirements.txt && apk del --no-cache git

COPY . /app

EXPOSE 8001
CMD gunicorn --log-level debug -c /app/gunicorn_config.py server:app
